<?php require("inc/header.php"); ?>
<?php include("inc/config.php"); ?>
<div class="panel-body">
  <?php
    if (!empty($_GET['message']) && $_GET['message'] == 'saved') {
      echo '<div class="alert alert-dismissible alert-success">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success !</strong> Data berhasil disimpan.
  </div>';
    } else if (!empty($_GET['message']) && $_GET['message'] == 'updated') {
      echo '<div class="alert alert-dismissible alert-warning">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success !</strong> Data berhasil diperbarui.
  </div>';
    } else if (!empty($_GET['message']) && $_GET['message'] == 'deleted') {
      echo '<div class="alert alert-dismissible alert-danger">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success !</strong> Data berhasil dihapus.
  </div>';
    }
  ?>
  <a href="https://gitlab.com/kuliah-unp/bprog-tgs-prakt-1-crud/tree/master" target="_blank" class="btn btn-primary pull-right"><i class="fa fa-code"></i> Source code</a>
  <a href="tambah.php" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
  <table class="table">
    <thead>
      <th>No</th>
      <th>Total Uang</th>
      <th>Kelebihan Ratih</th>
      <th>Uang Ratih</th>
      <th>Action</th>
    </thead>
    <tbody>
      <?php
        $query = mysql_query("SELECT * FROM `soal-13`");
        $nomor = 1;
        while ($data = mysql_fetch_array($query)) {
      ?>
        <tr>
          <td><?php echo $nomor ?></td>
          <td><?php echo $data['total_uang'] ?></td>
          <td><?php echo $data['kelebihan_ratih'] ?></td>
          <td><?php echo $data['uang_ratih'] ?></td>
          <td>
            <a href="edit.php?no=<?php echo $data['no']; ?>" title="Edit">
              <i class="fa fa-edit"></i>
            </a>
            <a href="proses/delete.php?no=<?php echo $data['no']; ?>" title="Hapus">
              <i class="fa fa-trash"></i>
            </a>
          </td>
        </tr>
      <?php
          $nomor++;
        }
      ?>
    </tbody>
  </table>
</div>
<?php require("inc/footer.php"); ?>
