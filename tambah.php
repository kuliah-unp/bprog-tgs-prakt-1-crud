<?php require("inc/header.php"); ?>
<div class="panel-body">
  <form action="proses/insert.php" method="POST" class="form">
    <a href="index.php" class="btn btn-default pull-right"><i class="fa fa-th-large"></i> Lihat data</a>
    <p>Uang Ratih <b><u>lebih besar</u></b> dibanding milik Jaka.</p>
    <div class="form-group">
      <label>Total uang Ratih dan Jaka (Rp) :</label>
      <input type="text" name="total_uang" class="form-control" placeholder="Masukkan total uang keduanya" required="true">
    </div>
    <div class="form-group">
      <label>Selisih uang Ratih dan Jaka (Rp) :</label>
      <input type="text" name="kelebihan_ratih" class="form-control" placeholder="Masukkan selisih uang mereka" required="true">
    </div>
    <div class="text-center">
      <input type="submit" value="Hitung Uang Ratih !" class="btn btn-primary">
    </div>
  </form>
</div>
<?php require("inc/footer.php"); ?>
