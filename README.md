# Bahasa Pemrograman
## Tugas Praktikum 1 - CRUD

Membuat CRUD (*Create, Read, Update, Delete*) sederhana berdasarkan soal matematika berikut ini :

![soal13](assets/img/13.jpg)

## Demo
Lihat demo online [disini](http://demo.tammam.id/bprog-tgs-prakt-1-crud/)

## Anggota Kelompok
- Aditya Gusti Tammam (14.1.03.03.0052)
- Dwy Novia Ulin Nuha (14.1.03.03.0046)
- Muhammad Irfan Aziz (14.1.03.03.0035)
