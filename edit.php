<?php

require("inc/header.php");
include("inc/config.php");

$no = $_GET['no'];

$query = mysql_query("SELECT * FROM `soal-13` WHERE no='$no'") or die(mysql_error());

$data = mysql_fetch_array($query);
?>
<div class="panel-body">
  <form action="proses/update.php" method="POST" class="form">
    <input type="hidden" name="no" value="<?php echo $data['no']; ?>" />
    <a href="index.php" class="btn btn-default pull-right"><i class="fa fa-th-large"></i> Lihat data</a>
    <p>Uang Ratih <b><u>lebih besar</u></b> dibanding milik Jaka.</p>
    <div class="form-group">
      <label>Total uang Ratih dan Jaka (Rp) :</label>
      <input type="text" name="total_uang" class="form-control" placeholder="Masukkan total uang keduanya" required="true" value="<?php echo $data['total_uang']; ?>">
    </div>
    <div class="form-group">
      <label>Selisih uang Ratih dan Jaka (Rp) :</label>
      <input type="text" name="kelebihan_ratih" class="form-control" placeholder="Masukkan selisih uang mereka" required="true" value="<?php echo $data['kelebihan_ratih']; ?>">
    </div>
    <div class="form-group">
      <label>Uang Ratih (Rp) :</label>
      <input type="text" class="form-control" value="<?php echo $data['uang_ratih']; ?>" disabled>
    </div>
    <div class="text-center">
      <input type="submit" value="Hitung Ulang Uang Ratih !" class="btn btn-primary">
    </div>
  </form>
</div>
<?php require("inc/footer.php"); ?>
