<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Tugas Praktikum 1 - Soal no. 13</title>
  <!-- Bootstrap 3.3.6 CSS -->
  <link rel="stylesheet" type="text/css" href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="assets/bower_components/fontawesome/css/font-awesome.min.css">
</head>
<body>
  <div class="container">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">
          <b>
              <span>Tugas Praktikum 1 | Soal No. 13</span>
              <span class="pull-right">Bahasa Pemrograman | Kelas 2A</span>
          </b>
          <h1 class="text-center">Uang Ratih dan Jaka</h1>
        </div>
